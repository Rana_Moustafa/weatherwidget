Weather Widget
---------------

- This code represents the source code used to impelement a weather widget

- The data shown are real data from MetaWeather API

- Ajax GET request is used to get the real data

- Then this data was displayed in the interface of the app

- The widget show the temprature of New York City for 6 days including today

- The weather status change the icons every time

- There is a loading icon shown at the loading of the widget then it disappears when the request is successful

- The widget show the Temps for 6 days including today

- Each day in the bottom show min and max Temp with the corresponding icon

- At the top part shows TODAY's Temps with the icon and the CURRENT Temp