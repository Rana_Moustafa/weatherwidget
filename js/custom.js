$(document).ready(function(){
	var weekday = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
	var weatherDays = [];
	var weatherIcon;
	var newIcon;
	var weatherTodayDate;
	var weatherToday = false;



	// ajax request to get the weather data form the metaweather API
	$.ajax({
        url: "https://www.metaweather.com/api/location/location/2459115/",
        type: 'GET',
        crossDomain : true,
        cors: true ,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': "GET, POST, HEAD"
         },
   
        dataType: 'json', // added data type
        success: function(res) {
            //console.log(JSON.stringify(res));
            //console.log(res.consolidated_weather[0]);

            // hide loading icon 
        	$('.fa-spin').hide();
        	

        	// loop through the result object 
        	// to extract the needed data
        	//and push it into the new object

            $.each( res.consolidated_weather, function( key, value ){
            	
            	
			
				//console.log(weatherDays);
				//console.log(value.weather_state_name);

				// assign different icon to each weather status
				switch (value.weather_state_name){
					case 'Snow':
					newIcon = 'wi-snow';
					break;
					case 'Sleet':
					newIcon = 'wi-sleet';
					break;
					case 'Hail':
					newIcon = 'wi-hail';
					break;
					case 'Thunder':
					newIcon = 'wi-thunderstorm';
					break;
					case 'Heavy Rain':
					newIcon = 'wi-day-hail';
					break;
					case 'Light Rain':
					newIcon = 'wi-day-snow';
					break;
					case 'Showers':
					newIcon = 'wi-showers';
					break;
					case 'Heavy Cloud':
					newIcon = 'wi-cloudy';
					break;
					case 'Light Cloud':
					newIcon = 'wi-cloud';
					break;
					case 'Clear':
					newIcon = 'wi-day-sunny';
					break;

				}

				//console.log(value.applicable_date);


            	

				//console.log(weekday[getDayName.getDay()]);

				// check if the date if TODAY 
				var todayDate = (new Date()).getDate();
				var weatherDate = (new Date(value.applicable_date)).getDate();
				//console.log(todayDate);
				//console.log(weatherDate);
				if (todayDate === weatherDate){
					console.log('yessss');
					weatherToday = true;
					console.log(weatherTodayDate);
				}
				else{
					weatherToday = false;
				}

				// get the waether date
				// then use it to generate the corresponding week day name
				var getDayName = new Date(value.applicable_date);

				// create the new object with needed data to be used to update the ui
				// will get the week days names and convert Tempreture to celsius
				// and check if the date is for today's date 
				// get the weather status to change the weather icon
				weatherDays.push({ 
					id: key,
					day: weekday[getDayName.getDay()] , 
					currentTemp: Math.ceil(((value.the_temp)-32) / 1.8),
					maxTemp: Math.ceil(((value.max_temp)-32) / 1.8),
					minTep: Math.ceil(((value.min_temp)-32) / 1.8),
					weatherIcon: newIcon,
					weatherTodayDate: weatherToday,
					weatherStatusName: value.weather_state_name
				});
				
				//console.log(weatherDays);
				//console.log(value.the_temp);
				//console.log(((value.the_temp)-32) / 1.8);	
            });
            //console.log(weatherDays);

            // loop through the new object to use the data to update the ui with the new tempratures and icons
            // we will update all the day names and icons and min/max temp.
            $.each( weatherDays, function( key, value ){
            	var selectedItem = $('.day-weather ul li:nth-child(1)')[key];
            	$(selectedItem).find('.wi').removeClass('wi-refresh');
            	$(selectedItem).find('.wi').addClass( value.weatherIcon );
            	$('.day-weather ul li:nth-child(2)')[key].innerHTML = (value.day);
                $('.day-weather ul li:nth-child(3) strong')[key].innerHTML = (value.maxTemp);
                $('.day-weather ul li:nth-child(3) span')[key].innerHTML = (value.minTep);

                // check if the date is today (true)
                // then update today's data
                if(value.weatherTodayDate){
                   	$('.weather-today .weather-today-block:nth-child(1) .wi').addClass(value.weatherIcon);
                	$('.weather-today .weather-today-block:nth-child(1) span').html(value.weatherStatusName);
                	$('.weather-today .weather-today-block:nth-child(3) .temp-degree').html(value.currentTemp);
                	$('.weather-today-block h1').html( res.parent.title );
                	console.log('todayIcon');
                	console.log(value.weatherTodayDate);
                }
            });
        },
        error: function (textStatus, errorThrown) {
            console.log(errorThrown,textStatus );
        }
    });
});